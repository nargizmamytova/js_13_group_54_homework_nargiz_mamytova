import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FieldComponent } from './field/field.component';
import { BoxComponent } from './field/box/box.component';
import {FormsModule} from "@angular/forms";
import { BtnsComponent } from './btns/btns.component';

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    BoxComponent,
    BtnsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
