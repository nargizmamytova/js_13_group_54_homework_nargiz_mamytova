import {Component} from '@angular/core';
import {Box} from "../shared/box.module";


@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent {
  box = '';
  boxes: Box[] = [];
  tries = 0;
  message = '';
  constructor() {
    this.boxes = this.onCreateBox();
  }
  onCreateBox() {
    const ring = Math.floor(Math.random()*(36 + 1))
    for(let i = 0; i < 36; i++){
     if( ring === i ){
       this.box = '0'
     }else {
       this.box = '';
     }
     this.boxes.push(new Box(this.box));
    }
    return this.boxes
  }

  onClickBtn(i: number) {
    if(this.box[i] === '0'){
      this.message = ` Вы нашли за ${this.tries} попыток`;
    }else {
      this.tries += 1;
    }
  }
  onReset(){
    this.boxes = [];
    this.tries = 0;
    this.onCreateBox();
  }
}









