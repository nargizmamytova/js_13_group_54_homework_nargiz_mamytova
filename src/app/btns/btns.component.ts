import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-btns',
  templateUrl: './btns.component.html',
  styleUrls: ['./btns.component.css']
})
export class BtnsComponent {
  @Input() tries: number = 0;
  @Output() reset = new EventEmitter();

  onResetBtn(){
    this.reset.emit();
  }
}
