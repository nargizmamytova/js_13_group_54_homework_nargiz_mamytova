import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent {
  @Input() box = '';
  @Output() createBox = new EventEmitter();
  bgrnd = '';

  onBoxChange(event: Event) {
    this.bgrnd = 'boxOpen';
    this.createBox.emit();
   }
}
